FactoryGirl.define do
  factory :team, aliases: [:home_team, :away_team] do
    name 'Junior'
    initials 'ATL J'
    nickname 'Tiburon'
    founded_at  { 21.years.ago }
    country 'CO'
    city 'Barranquilla'
  end
end
