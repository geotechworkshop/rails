FactoryGirl.define do
  factory :match do
    home_team
    away_team
    state 'scheduled'

    Match::STATES.each do |state_name|
      trait state_name do
        state state_name
      end
    end
  end
end
