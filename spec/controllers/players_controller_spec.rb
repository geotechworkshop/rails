require 'rails_helper'

RSpec.describe PlayersController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Player. As you add validations to Player, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    { nickname: 'Messi'}
  }

  let(:invalid_attributes) {
    {}
  }

  describe "GET #index" do
    it "assigns all players as @players" do
      player = Player.create! valid_attributes
      get :index, params: {}, format: :json
      expect(assigns(:players)).to eq([player])
    end
  end

  describe "GET #show" do
    it "assigns the requested player as @player" do
      player = Player.create! valid_attributes
      get :show, params: {id: player.to_param}
      expect(assigns(:player)).to eq(player)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Player" do
        expect {
          post :create, params: {player: valid_attributes}, format: :json
        }.to change(Player, :count).by(1)
      end

      it "assigns a newly created player as @player" do
        post :create, params: {player: valid_attributes}, format: :json
        expect(assigns(:player)).to be_a(Player)
        expect(assigns(:player)).to be_persisted
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved player as @player" do
        post :create, params: {player: invalid_attributes}, format: :json
        expect(assigns(:player)).to be_a_new(Player)
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested player" do
        player = Player.create! valid_attributes
        put :update, params: {id: player.to_param, player: new_attributes}, format: :json
        player.reload
        skip("Add assertions for updated state")
      end

      it "assigns the requested player as @player" do
        player = Player.create! valid_attributes
        put :update, params: {id: player.to_param, player: valid_attributes}, format: :json
        expect(assigns(:player)).to eq(player)
      end
    end

    context "with invalid params" do
      it "assigns the player as @player" do
        player = Player.create! valid_attributes
        put :update, params: {id: player.to_param, player: invalid_attributes}, format: :json
        expect(assigns(:player)).to eq(player)
      end

      it "re-renders the 'edit' template" do
        player = Player.create! valid_attributes
        put :update, params: {id: player.to_param, player: invalid_attributes}, format: :json
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested player" do
      player = Player.create! valid_attributes
      expect {
        delete :destroy, params: {id: player.to_param}, format: :json
      }.to change(Player, :count).by(-1)
    end
  end
end
