require 'rails_helper'

RSpec.describe Tournament, type: :model do
  subject{ Tournament.new(name: 'Copa Libertadores', state: 'unpublished') }

  describe 'relationship' do
    it { should have_many(:addresses).dependent(:destroy) }
    it { should have_many(:teams) }
    it { should have_many(:tournament_teams) }
    it { should have_many(:teams).through(:tournament_teams) }
  end

  describe 'validations' do
    it{ should validate_presence_of(:name) }
    it{ should validate_presence_of(:state) }
  end

# it "not be valid" do
#   tournament = Tournament.new
#   expect(tournament.save).to eq(false)
# end

# it 'is not valid without name' do
#   subject.name = ''
#   expect(subject.valid?).to eq(false)
# end

# it 'is not valid without state' do
#   subject.state = nil
#   expect(subject.valid?).to eq(false)
# end

# it 'is valid with valid attrs' do
#   expect(subject.valid?).to eq(true)
# end
end
