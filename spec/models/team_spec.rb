require 'rails_helper'

RSpec.describe Team, type: :model do
  describe 'relationship' do
    it { should have_many(:tournament_teams) }
    it { should have_many(:tournaments).through(:tournament_teams) }
  end

end
