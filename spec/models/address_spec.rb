require 'rails_helper'

RSpec.describe Address, type: :model do
  subject{ Address.new(country: 'US') }
  describe 'relationship' do
    it{ should belong_to(:tournament) }
  end

  describe 'validations' do
    it{ should validate_inclusion_of(:country).in_array(['US', 'CO']) }
  end

  describe '#country_name' do
    it 'returns proper country proper' do
      subject.country = 'CO'
      expect(subject.country_name).to eq('Colombia')
    end
  end
end
