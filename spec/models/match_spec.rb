require 'rails_helper'

RSpec.describe Match, type: :model do
  describe 'relationships' do
    it{ should belong_to(:home_team).class_name('Team') }
    it{ should belong_to(:away_team).class_name('Team') }
  end

  describe 'validations' do
    it{ should validate_inclusion_of(:state).in_array(Match::STATES) }
  end

  describe '.by_team' do
    before(:each) do
      @team = FactoryGirl.create(:team)
    end

    it 'return home and away matches' do
      match1 = create(:match, home_team_id: @team.id)
      match2 = create(:match, away_team_id: @team.id)
      match3 = create(:match)
      matches = Match.by_team(@team.id)
      expect(matches).to include(match1)
      expect(matches).to include(match2)
      expect(matches).to_not include(match3)
    end
  end
end
