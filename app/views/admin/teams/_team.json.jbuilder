json.name team.name
json.initials team.initials
json.nickname team.nickname
json.founded_at team.founded_at
json.tournament do
  json.name @tournament.name
end
