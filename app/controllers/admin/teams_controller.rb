class Admin::TeamsController < ApplicationController
  before_action :set_tournament

  def index
    @teams = @tournament.teams
  end

  def create
    @team = @tournament.teams.build(team_params)
    if @team.save
      render :show, status: :created
    else
      render json: @team.errors, status: :unprocessable_entity
    end
  end

  def show
  end

  def update
  end

  def delete
  end

  private

  def set_tournament
    @tournament = Tournament.find(params[:tournament_id])
  end

  def team_params
    params.require(:team).permit(:name, :nickname)
  end
end
