class SecondaryBase < ActiveRecord::Base
  establish_connection(:secondary_database)
  self.abstract_class = true
end
