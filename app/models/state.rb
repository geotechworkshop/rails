class State < SecondaryBase
  self.table_name = 'gs.departamento'
  self.primary_key = :codigo
  belongs_to :country, foreign_key: :cod_pais
end
