class Match < ActiveRecord::Base
  STATES = %w(scheduled started in_progress ended)

  belongs_to :home_team, class_name: 'Team'
  belongs_to :away_team, class_name: 'Team'

  validates :state, inclusion: { in: STATES }

  #scope :ended, -> { where(state: 'ended') }

  STATES.each do |state|
    scope state, -> { where(state: state) }
  end

  def self.by_team(team_id)
    where('home_team_id = ? OR away_team_id = ?', team_id, team_id)
  end
end
