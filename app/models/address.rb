class Address < ActiveRecord::Base
  belongs_to :tournament
  validates :country, inclusion: { in: COUNTRIES.keys }

  def country_name
    COUNTRIES[country]['name']
  end
end
