class Country < SecondaryBase
  self.table_name = 'gs.pais'
  self.primary_key = :codigo
  has_many :states, foreign_key: :cod_pais
end
