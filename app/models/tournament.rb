class Tournament < ActiveRecord::Base
  validates :name, :state, presence: true
  has_many :addresses, dependent: :destroy
  has_many :tournament_teams
  has_many :teams, through: :tournament_teams
end
