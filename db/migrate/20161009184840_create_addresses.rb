class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :line1
      t.string :line2
      t.string :zip_code
      t.string :country
      t.string :state
      t.string :city
      t.references :tournament, index: true

      t.timestamps null: false
    end
    add_foreign_key :addresses, :tournaments
  end
end
