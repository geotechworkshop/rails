class AddScoreColumnToMatch < ActiveRecord::Migration
  def change
    add_column :matches, :home_score, :integer, default: 0
    add_column :matches, :away_score, :integer, default: 0
  end
end
