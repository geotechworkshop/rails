class CreateTournamentTeams < ActiveRecord::Migration
  def change
    create_table :tournament_teams do |t|
      t.references :tournament, index: true
      t.references :team, index: true

      t.timestamps null: false
    end
    add_foreign_key :tournament_teams, :tournaments
    add_foreign_key :tournament_teams, :teams
  end
end
