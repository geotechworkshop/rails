class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :home_team_id
      t.integer :away_team_id
      t.datetime :start_time
      t.string :state

      t.timestamps null: false
    end

    add_index :matches, :home_team_id
    add_index :matches, :away_team_id
  end
end
